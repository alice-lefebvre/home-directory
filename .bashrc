#
# ~/.bashrc
#

export XKB_DEFAULT_LAYOUT=fr
export MOZ_ENABLE_WAYLAND=1

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

normal="\[\e[39m\]"
red="\[\e[31m\]"
green="\[\e[32m\]"
yellow="\[\e[33m\]"
gray="\[\e[37m\]"

alias vi=vim
alias ls='ls --color=auto'
alias ll='ls -laoi'
alias grep='grep --color=auto'
# PS1='[\u@\h \W]\$ '

DATE="$gray[$(date +'%H:%M')]"
# _PADDING="$(($(tput cols) - 7))"
# PS1_RIGHT="$(printf %${_PADDING}s)$DATE"
# not possible in bash :(
PS1="$red[$green\u $yellow\w$red]\$ $normal"
# PS1="\[$PS1_RIGHT\r\]$red[$green\u $yellow\w$red]\$ $normal"
# \[ & \] needed around non printable characters
