#!/bin/bash

frmt_date=$(date +'%A, %B %d %k:%M')

if [ -d "/proc/acpi/button/lid" ]; then
    # Laptop
    vol=$(pamixer --get-volume-human)
    plugged=$(cat /sys/class/power_supply/AC/online)
    if [ $plugged -eq 0 ]
    then
        power_now0=$(cat /sys/class/power_supply/BAT0/power_now)
        power_now1=$(cat /sys/class/power_supply/BAT1/power_now)
        power_now=$(expr $power_now0 + $power_now1)
        power_now=$(printf "%.1fW" "$((10**2 * $power_now / 10**6))e-2")
    else
        power_now="charging"
    fi
    bat0_prct=$(cat /sys/class/power_supply/BAT0/capacity)
    bat1_prct=$(cat /sys/class/power_supply/BAT1/capacity)
    light=$(brightnessctl |grep 'Current brightness' |sed 's/.*(\(.*\))/\1/') 

    echo -e "$power_now | batteries: $bat0_prct%, $bat1_prct% | brightness: $light | vol: $vol | $frmt_date "
else
    # Desktop
    vol=$(amixer sget Master | grep 'Right:' | awk -F'[][]' '{ print $2 }')
    echo -e "vol: $vol | $frmt_date "
fi
