-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

-- empty setup using defaults
require("nvim-tree").setup({
    renderer = {
        icons = {
            glyphs = {
                default = "",
                symlink = "",
                bookmark = "",
                modified = "M",
                folder = {
                    arrow_closed = ">",
                    arrow_open = "v",
                    default = "",
                    open = "",
                    empty = "",
                    empty_open = "",
                    symlink = "",
                    symlink_open = "",
                },
                git = {
                    unstaged = "U",
                    unmerged = "U",
                    staged = 'A',
                    renamed = 'R',
                    untracked = "x",
                    deleted = 'D',
                    ignored = '',
                }
            }
        }
    }
})
