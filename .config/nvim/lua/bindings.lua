local key = vim.keymap.set
local noremap = { noremap = true }

-- key("n", "<C-a>", ":NvimTreeToggle<CR>", noremap)
key("n", "p", "pv=", noremap)
key("n", "P", "Pv=", noremap)
key("n", "<C-d>", "<C-d>zz", noremap)
key("n", "<C-u>", "<C-u>zz", noremap)
-- escape to escape terminal mode
key("t", "<Esc>", "<C-\\><C-n>", noremap)

vim.opt.splitright = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.guicursor = ""

vim.opt.autoindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true
--vim.opt.scrolloff = 6

vim.opt.incsearch = true

-- Diagnostic keymaps
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)

vim.keymap.set('i', '<C-c>', '<Esc>')
