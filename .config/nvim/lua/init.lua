vim.g.mapleader = " "
vim.g.maplocalleader = " "

require('plugins');
-- require('coc');
-- c nul
require('nvim_tree');
require('lsp_setup');
require('autocomplete_setup');
require('telescope_setup');

require'nvim-treesitter.configs'.setup {
    -- A list of parser names, or "all" (the five listed parsers should always be installed)
    ensure_installed = { "c", "rust", "lua" },
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false
    },

    indent = {
        enable = true,
    }

}
-- Gitsigns
require('gitsigns').setup {
    signcolumn = true,
    sign_priority = 6,
    signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
    },
    on_attach = function(bufnr)
        vim.keymap.set('n', '[c', require('gitsigns').prev_hunk, { buffer = bufnr })
        vim.keymap.set('n', ']c', require('gitsigns').next_hunk, { buffer = bufnr })
    end,
}

-- blankline
require('indent_blankline').setup {
    show_trailing_blankline_indent = false,
    show_current_context = true,
    use_treesitter = true,
    --char = ':'
    -- show_current_context_start = true,
}

-- set color theme
require('catppuccin').setup({
    flavour = "macchiato", -- latte, frappe, macchiato, mocha
    no_italic = true,
    integrations = {
        cmp = true,
        treesitter = true,
        native_lsp = {
            enabled = true,
            inlay_hints = {
                background = true
            }
        }
    },
})

-- change made for dayfox
local groups = {
    dayfox = {
        CursorLine = { bg = "palette.white" }
    },
    duskfox = {
        CursorLine = { bg = "palette.white" }
    },
    nightfox = {
        -- CursorLine = { bg = "palette.blue" }
    },
}
require('nightfox').setup({
    options = {
        styles = {
            -- comments = "italic",
            keywords = "bold",
            types = "bold",
        }
    },
    groups = groups, --comment for dark themes
})

-- require('nightfox').init()
-- local override = require('nightfox').override
-- vim.cmd.colorscheme "catppuccin"
vim.cmd.colorscheme "nightfox"
-- correcting theme
-- local C = require("catppuccin.palettes").get_palette()
-- local white = "#D9E0EE" -- from https://github.com/catppuccin/catppuccin/blob/main/docs/translation-table.md
-- local black4 = "#575268"
-- types to play with found there: https://github.com/catppuccin/nvim/tree/main/lua/catppuccin/groups
-- vim.api.nvim_set_hl(0, 'CocInlayHint', { fg = C.overlay0, bg = C.surface0 })
-- vim.api.nvim_set_hl(0, 'CursorLineNr', { fg = C.peach })
-- keymaps
require('bindings');
